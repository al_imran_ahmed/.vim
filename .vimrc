"Execute pathogen runtime path manipulation
execute pathogen#infect()

"Show line number of row
set number
 
"Limit the Text window with 80 character
set colorcolumn=80

"Indent automatically depends on file type
filetype indent on
set autoindent smartindent

"Set syntext on 
syntax on
set hlsearch incsearch title ruler

"Case insensitive search
set ic

" Wrap text instead of being on one line
set lbr

" Some recomanded settings for syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"Start NERDTree when vim start
autocmd vimenter * NERDTree
"Open directory of currenly oppend file in NERDTree
" Open NERDTree in the directory of the current file (or /home if no file is open)
nmap <silent> <C-i> :call NERDTreeToggleInCurDir()<cr>
function! NERDTreeToggleInCurDir()
	" If NERDTree is open in the current buffer
	if (exists("t:NERDTreeBufName") && bufwinnr(t:NERDTreeBufName) != -1)
		exe ":NERDTreeClose"
	else
		exe ":NERDTreeFind"
	endif
endfunction

" size of a hard tabstop
set tabstop=4

" size of an "indent"
set shiftwidth=4

" a combination of spaces and tabs are used to simulate tab stops at a width
" other than the (hard)tabstop
set softtabstop=4
